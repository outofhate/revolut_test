rootProject.buildFileName = "build.gradle.kts"
pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
    }

    resolutionStrategy {
        eachPlugin {
            when (requested.id.id) {
                "com.android.application", "com.android.library" -> {
                    println("${requested.version} ${requested.id.id}")
                    useModule("com.android.tools.build:gradle:${requested.version}")

                }
            }
        }
    }
}
include(":app")
