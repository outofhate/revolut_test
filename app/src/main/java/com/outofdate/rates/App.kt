package com.outofdate.rates

import android.support.multidex.MultiDexApplication
import com.outofdate.rates.base.internal.AppComponent
import com.outofdate.rates.base.internal.ComponentsHandler
import kotlin.reflect.KClass

class App : MultiDexApplication(), ComponentsHandler {

    override fun onCreate() {
        super.onCreate()
        val component = AppComponent.Initializer.init(this)
        component.inject(this)
        addComponent(component)
    }

    override fun <C : Any> addComponent(component: C): C {
        component::class.simpleName?.run {
            replace("Dagger", "").replace("Impl", "").run {
                if (!hasComponent(this)) {
                    components[this] = component
                }
            }
        }
        return component
    }

    internal var components: HashMap<String?, Any?> = HashMap()

    override fun <C : Any> hasComponent(component: KClass<C>): Boolean {
        return hasComponent(component.simpleName)
    }

    override fun hasComponent(component: String?): Boolean = components.contains(component)

    override fun <C : Any> getComponent(component: KClass<C>): C? = components[component.simpleName] as C?

    override fun <C : Any> removeComponent(component: KClass<C>) {
        components.remove(component.simpleName)
    }

}
