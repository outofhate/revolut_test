package com.outofdate.rates.base.mvp

import io.reactivex.disposables.CompositeDisposable

abstract class AbsMvpPresenter<V : Mvp.View, I : Mvp.Interactor>(protected var interactor: I) : Mvp.Presenter<V, I> {

    protected var view: V? = null
    protected val disposables = CompositeDisposable()

    override fun bind(view: V) {
        super.bind(view)
        this.view = view
    }

    override fun unbind() {
        super.unbind()
        this.view = null
        disposables.clear()
    }

    inline fun view(block: V.() -> Unit) {
        view?.block()
    }
}
