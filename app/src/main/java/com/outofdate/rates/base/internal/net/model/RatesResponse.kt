package com.outofdate.rates.base.internal.net.model

import java.util.*

data class RatesResponse(val base: String, val rates: HashMap<String, Float>)
