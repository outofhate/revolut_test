package com.outofdate.rates.base.internal

import android.app.Application
import android.content.Context
import android.databinding.ViewDataBinding
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.view.View
import com.outofdate.rates.base.epoxy.BaseEpoxyModel
import com.outofdate.rates.base.internal.InjectorExtensions.getMainActivityComponent
import com.outofdate.rates.base.internal.InjectorExtensions.getRatesComponent
import com.outofdate.rates.modules.main.DaggerMainActivityComponent
import com.outofdate.rates.modules.main.MainActivityComponent
import com.outofdate.rates.modules.rates.DaggerRatesComponent
import com.outofdate.rates.modules.rates.RatesComponent
import dagger.Component
import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation

object InjectorExtensions {

    fun Context.getMainActivityComponent(): MainActivityComponent = DaggerMainActivityComponent.builder().appComponent(getAppComponent()).build()
    fun Context.getRatesComponent(): RatesComponent = DaggerRatesComponent.builder().appComponent(getAppComponent()).build()

}


fun <C : Any> ViewDataBinding.inject(clazz: KClass<C>, injector: C.() -> Unit) {
    injector.run {
        context().inject(clazz, this)
    }
}

fun <C : Any> Context.inject(clazz: KClass<C>, injector: C.() -> Unit) {
    setupComponent(clazz)?.let {
        injector(it)
    }
}

fun <C : Any> Context.injectWithSubcomponents(clazz: KClass<C>, injector: ComponentsHandler.(C) -> Unit) {
    val c = setupComponent(clazz)
    if (c != null) {
        injector(getComponents(), c)
    }
}

fun <C : Any> View.inject(clazz: KClass<C>, injector: C.() -> Unit) {
    context.inject(clazz, injector)
}

fun <C : Any> View.injectWithSubcomponents(clazz: KClass<C>, injector: ComponentsHandler.(C) -> Unit) {
    context?.injectWithSubcomponents(clazz, injector)
}

fun <C : Any> Fragment.inject(clazz: KClass<C>, injector: C.() -> Unit) {
    context?.run {
        val c = setupComponent(clazz)
        if (c != null) {
            injector(c)
        }
    }
}

fun <C : Any> Fragment.injectWithSubcomponents(clazz: KClass<C>, injector: ComponentsHandler.(C) -> Unit) {
    context?.injectWithSubcomponents(clazz, injector)

}

fun <C : Any> BaseEpoxyModel<*, *>.inject(clazz: KClass<C>, injector: C.() -> Unit) {
    binding?.inject(clazz, injector)
}

fun <C : Any> Context.setupComponent(clazz: KClass<C>): C? {
    return if (!getComponents().hasComponent(clazz)) {
        val component = componentByClass(clazz)
        component?.run {
            getComponents().addComponent(this)
        } as C?
    } else getComponents().getComponent(clazz)!!
}

fun Context.getComponents(): ComponentsHandler = when (this) {
    is Application -> this as ComponentsHandler
    else -> applicationContext as ComponentsHandler
}

fun Context.componentByClass(clazz: KClass<*>): Any? {
    return clazz.findAnnotation<Component>()?.let {
        return when (clazz) {
            MainActivityComponent::class -> getMainActivityComponent()
            RatesComponent::class -> getRatesComponent()
            else -> null

        }
    }
}

fun Context.removeComponent(clazz: KClass<*>) {
    getComponents().removeComponent(clazz)
}

fun Fragment.removeComponent(clazz: KClass<*>) {
    context?.getComponents()?.removeComponent(clazz)
}

fun View.removeComponent(clazz: KClass<*>) {
    context.removeComponent(clazz)
}

fun ViewDataBinding.removeComponent(clazz: KClass<*>) {
    context().removeComponent(clazz)
}

fun ViewDataBinding.context() = this.root.context

fun ViewDataBinding.getString(@StringRes res: Int): String = context().getString(res)

private fun Context.getAppComponent(): AppComponent {
    return getComponents().run {
        return when {
            !hasComponent(AppComponent::class) -> addComponent(DaggerAppComponent.builder().build()) as AppComponent
            else -> getComponent(AppComponent::class)!!
        }
    }
}