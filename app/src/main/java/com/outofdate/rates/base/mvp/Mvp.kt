package com.outofdate.rates.base.mvp

interface Mvp {

    interface View {
        fun showLoader()
        fun hideLoader()
        fun showError(message: String?)

    }

    interface Presenter<V : View, I : Interactor> {

        fun bind(view: V) = Unit
        fun unbind() = Unit
    }

    interface Interactor {
    }
}

/**
 * Hide loader extension
 */
fun Mvp.View?.hl() {
    this?.hideLoader()
}

/**
 * Show loader extension
 */
fun Mvp.View?.sl() {
    this?.showLoader()
}
