package com.outofdate.rates.base.internal

import kotlin.reflect.KClass

interface ComponentsHandler {
    fun <C : Any> addComponent(component: C): C
    fun <C : Any> hasComponent(component: KClass<C>): Boolean
    fun hasComponent(component: String?): Boolean
    fun <C : Any> getComponent(component: KClass<C>): C?
    fun <C : Any> removeComponent(component: KClass<C>)
}
