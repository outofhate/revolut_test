package com.outofdate.rates.base.internal

import android.app.Application
import com.outofdate.rates.App
import com.outofdate.rates.base.internal.android.AndroidModule
import com.outofdate.rates.base.internal.android.AppModule
import com.outofdate.rates.base.internal.android.GsonModule
import com.outofdate.rates.base.internal.android.SchedulersModule
import com.outofdate.rates.base.internal.net.NetworkConfigModule
import com.outofdate.rates.base.internal.net.OkHttpModule
import com.outofdate.rates.base.internal.net.RetrofitModule
import dagger.Component
import io.reactivex.Scheduler
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidModule::class,
        AppModule::class,
        OkHttpModule::class,
        RetrofitModule::class,
        NetworkConfigModule::class,
        SchedulersModule::class,
        GsonModule::class]
)
interface AppComponent {

    val app: Application

    fun retrofit(): Retrofit

    fun scheduler(): Scheduler

    fun inject(app: App)

    object Initializer {

        fun init(app: Application): AppComponent {
            return DaggerAppComponent.builder()
                .appModule(AppModule(app))
                .build()
        }
    }
}