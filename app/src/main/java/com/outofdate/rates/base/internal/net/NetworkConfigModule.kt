package com.outofdate.rates.base.internal.net

import dagger.Module
import dagger.Provides
import okhttp3.HttpUrl
import javax.inject.Singleton

@Module
class NetworkConfigModule {

    @Provides
    @Singleton
    fun baseUrl(builder: HttpUrl.Builder) = builder.build()

    @Provides
    @Singleton
    fun baseUrlBuilder(url: String) = HttpUrl.Builder().scheme("https").host(url)

    @Provides
    @Singleton
    fun url() = "revolut.duckdns.org"

}
