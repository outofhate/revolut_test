package com.outofdate.rates.base.epoxy

import android.content.Context
import android.databinding.ViewDataBinding
import com.airbnb.epoxy.DataBindingEpoxyModel

abstract class BaseEpoxyModel<DATA: Any, DB : ViewDataBinding> protected constructor(protected val data: DATA) : DataBindingEpoxyModel() {

    var binding: DB? = null
    protected var context: Context? = null

    override fun setDataBindingVariables(binding: ViewDataBinding) {
        this.binding = binding as DB
        bind(binding)
    }

    protected abstract fun bind(binding: DB)

    protected abstract fun unbind(binding: DB)

    protected fun attached(binding: DB) = Unit

    protected fun detached(binding: DB) = Unit

    override fun unbind(holder: DataBindingEpoxyModel.DataBindingHolder) {
        unbind(holder.dataBinding as DB)
        super.unbind(holder)
        binding = null
    }

    override fun onViewAttachedToWindow(holder: DataBindingEpoxyModel.DataBindingHolder) {
        attached(holder.dataBinding as DB)
        super.onViewAttachedToWindow(holder)
    }

    override fun onViewDetachedFromWindow(holder: DataBindingEpoxyModel.DataBindingHolder) {
        detached(holder.dataBinding as DB)
        super.onViewDetachedFromWindow(holder)
    }

    fun hasData(): Boolean {
        return data != null
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is BaseEpoxyModel<*, *>) return false
        if (!super.equals(other)) return false

        if (data != other.data) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + (data?.hashCode() ?: 0)
        return result
    }

}