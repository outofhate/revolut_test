package com.outofdate.rates.base.internal.net

import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BASIC
import timber.log.Timber.d
import javax.inject.Singleton

@Module
class OkHttpModule {

    @Provides
    @Singleton
    fun client(interceptor: Interceptor): OkHttpClient {
        val builder = OkHttpClient
                .Builder()
                .addInterceptor(HttpLoggingInterceptor { d(it) }.setLevel(BASIC))
                .addInterceptor(interceptor)
        return builder.build()
    }

    @Provides
    @Singleton
    fun interceptor(): Interceptor {
        return BaseInterceptor()
    }

    class BaseInterceptor : Interceptor {
        override fun intercept(chain: Interceptor.Chain): Response {
            return chain.proceed(chain.request().newBuilder()
                    .build())
        }
    }
}