package com.outofdate.rates.base.mvp

import android.databinding.ViewDataBinding
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast.LENGTH_SHORT
import android.widget.Toast.makeText
import com.outofdate.rates.base.android.BaseFragment
import javax.inject.Inject

abstract class MvpFragment<DB : ViewDataBinding, V : Mvp.View, P : Mvp.Presenter<V, out Mvp.Interactor>> : BaseFragment<DB>(), Mvp.View {
    @Inject
    lateinit var presenter: P

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        presenter.bind(this as V)
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.unbind()
    }

    override fun showError(message: String?) {
        if (context != null)
            makeText(context, message ?: "No message", LENGTH_SHORT).show()
    }

}

