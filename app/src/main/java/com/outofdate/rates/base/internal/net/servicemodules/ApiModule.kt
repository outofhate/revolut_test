package com.outofdate.rates.base.internal.net.servicemodules

import com.outofdate.rates.base.internal.net.services.RatesService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class ApiModule {

    @Provides
    fun ratesService(retrofit: Retrofit): RatesService {
        return retrofit.create(RatesService::class.java)
    }

}