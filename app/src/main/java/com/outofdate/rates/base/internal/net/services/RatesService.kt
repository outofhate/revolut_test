package com.outofdate.rates.base.internal.net.services

import com.outofdate.rates.base.internal.net.model.RatesResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface RatesService {

    @GET("latest")
    fun byBase(@Query("base") id: String?): Observable<RatesResponse?>

}