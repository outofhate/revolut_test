package com.outofdate.rates.base.internal.android

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AndroidModule {

    @Provides
    @Singleton
    fun getPrefs(context: Context): SharedPreferences {
        return context.getSharedPreferences(context.packageName.plus(".main"), Context.MODE_PRIVATE)
    }

}
