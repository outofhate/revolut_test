package com.outofdate.rates.base.mvp

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import android.widget.Toast.makeText
import javax.inject.Inject

abstract class MvpActivity<DB : ViewDataBinding, V : Mvp.View, P : Mvp.Presenter<V, out Mvp.Interactor>> : AppCompatActivity(), Mvp.View {
    @Inject
    lateinit var presenter: P

    lateinit var binding: DB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, layout)
        presenter.bind(this as V)
    }

    @get:LayoutRes
    abstract val layout: Int

    override fun onDestroy() {
        super.onDestroy()
        presenter.unbind()
    }

    override fun showError(message: String?) {
        if (!isFinishing)
            makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showLoader() {
    }

    override fun hideLoader() {
    }
}

