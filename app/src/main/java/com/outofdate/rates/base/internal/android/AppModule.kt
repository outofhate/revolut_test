package com.outofdate.rates.base.internal.android

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class AppModule(private val application: Application) {

    @Provides
    fun app(): Application {
        return application
    }

    @Provides
    fun appContext(): Context {
        return application
    }

}