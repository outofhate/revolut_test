package com.outofdate.rates.base.internal.android

import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.TestScheduler

import javax.inject.Named

@Module
class SchedulersModule {

    @Provides
    fun main(): Scheduler {
        return AndroidSchedulers.mainThread()
    }

    @Provides
    @Named("test")
    fun test(): Scheduler {
        return TestScheduler()
    }

}
