package com.outofdate.rates.base.android

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.graphics.Color
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.ViewGroup

abstract class BaseFragment<DB : ViewDataBinding> : Fragment() {

    lateinit var binding: DB
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    protected open fun setupExitTransitions(): Boolean {
        return true
    }

    protected open fun setupEnterTransitions(): Boolean {
        return true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): android.view.View? {
        retainInstance = true
        setHasOptionsMenu(true)
        binding = DataBindingUtil.inflate<DB>(inflater, layout, container, false).apply {
            if (root.background == null)
                root.setBackgroundColor(Color.WHITE)
        }
        return binding.root
    }

    @get:LayoutRes
    abstract val layout: Int

    open fun showLoader() = Unit

    open fun hideLoader() = Unit
}