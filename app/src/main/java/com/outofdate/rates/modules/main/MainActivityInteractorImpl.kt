package com.outofdate.rates.modules.main

import com.outofdate.rates.base.mvp.AbsMvpInteractor
import javax.inject.Inject

class MainActivityInteractorImpl @Inject
constructor() : AbsMvpInteractor(),
        MainActivityContract.Interactor
