package com.outofdate.rates.modules.rates

import com.outofdate.rates.R
import com.outofdate.rates.base.epoxy.BaseEpoxyModel
import com.outofdate.rates.base.internal.inject
import com.outofdate.rates.databinding.ItemRatesBinding
import com.outofdate.rates.modules.rates.model.Rate
import javax.inject.Inject

class RatesEpoxyModel(data: Rate) : BaseEpoxyModel<Rate, ItemRatesBinding>(data) {

    @Inject
    lateinit var presenter: RatesContract.Presenter

    init {
        id(data.name)
    }

    override fun bind(binding: ItemRatesBinding) {
        inject(RatesComponent::class) {
            inject(this@RatesEpoxyModel)
        }
        binding.name.text = data.name
        binding.value.setText((data.value * data.mp).toString())
        binding.root.setOnClickListener {
            presenter.getRates(data.name)
        }
    }

    override fun unbind(binding: ItemRatesBinding) {
        binding.root.setOnClickListener(null)
    }

    override fun getDefaultLayout() = R.layout.item_rates
}