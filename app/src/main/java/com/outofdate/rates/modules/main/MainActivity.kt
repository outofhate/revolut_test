package com.outofdate.rates.modules.main

import android.os.Bundle
import com.outofdate.rates.R
import com.outofdate.rates.base.internal.inject
import com.outofdate.rates.base.mvp.MvpActivity
import com.outofdate.rates.databinding.ActivityMainBinding
import com.outofdate.rates.modules.rates.RatesFragment

class MainActivity :
    MvpActivity<
            ActivityMainBinding,
            MainActivityContract.View,
            MainActivityContract.Presenter>(),
    MainActivityContract.View {
    override fun onCreate(savedInstanceState: Bundle?) {
        inject(MainActivityComponent::class) {
            inject(this@MainActivity)
        }
        super.onCreate(savedInstanceState)
        supportFragmentManager.beginTransaction()
            .replace(R.id.host, RatesFragment())
            .commitAllowingStateLoss()
    }

    override val layout: Int = R.layout.activity_main

}
