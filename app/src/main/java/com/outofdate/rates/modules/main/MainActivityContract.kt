package com.outofdate.rates.modules.main

import com.outofdate.rates.base.mvp.Mvp

interface MainActivityContract {

    interface View : Mvp.View

    interface Presenter : Mvp.Presenter<View, Interactor>

    interface Interactor : Mvp.Interactor


    @javax.inject.Scope
    @Retention(AnnotationRetention.RUNTIME)
    annotation class Scope
}

