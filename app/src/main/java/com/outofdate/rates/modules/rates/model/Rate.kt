package com.outofdate.rates.modules.rates.model

data class Rate(
    val base: String,
    val mp: Float,
    val name: String,
    val value: Float,
    val isBase: Boolean
)
