package com.outofdate.rates.modules.main

import com.outofdate.rates.base.internal.AppComponent
import dagger.Component

@MainActivityContract.Scope
@Component(dependencies = [AppComponent::class],
        modules = [
            MainActivityModule::class
        ]
)
interface MainActivityComponent {

    fun inject(activity: MainActivity)

}
