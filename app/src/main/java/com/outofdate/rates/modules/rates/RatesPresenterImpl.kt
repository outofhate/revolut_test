package com.outofdate.rates.modules.rates

import android.util.Log
import com.outofdate.rates.base.mvp.AbsMvpPresenter
import javax.inject.Inject

class RatesPresenterImpl @Inject constructor(interactor: RatesContract.Interactor) :
    AbsMvpPresenter<RatesContract.View, RatesContract.Interactor>(interactor),
    RatesContract.Presenter {

    init {
        interactor.subscribeUpdates()
            .doOnSubscribe { disposables.add(it) }
            .subscribe({
                view?.update(it)
            }, { Log.d("Tag", "error", it) })
    }

    override fun setMultiplier(mp: Float?) {
        val m = mp ?: 0f
        view?.setMultiplier(m)
        interactor.setMultiplier(m)
    }

    override fun getRates(base: String) {
        view?.toTop()
        interactor.getRates(base)
    }

}