package com.outofdate.rates.modules.rates

import com.outofdate.rates.base.internal.net.services.RatesService
import com.outofdate.rates.base.mvp.AbsMvpInteractor
import com.outofdate.rates.modules.rates.model.Rates
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class RatesInteractorImpl @Inject constructor(val ratesService: RatesService, val main: Scheduler) : AbsMvpInteractor(),
    RatesContract.Interactor {

    private var mp = 0f

    private val ratesNotifier = PublishSubject.create<Rates>()
    private var ratesDisposable: Disposable? = null
    override fun setMultiplier(m: Float) {
        mp = m
    }

    override fun subscribeUpdates(): Observable<Rates> {
        return ratesNotifier
            .observeOn(main)
    }

    override fun getRates(base: String){
        mp = 0f
        ratesDisposable?.dispose()
        ratesDisposable = ratesService.byBase(base)
            .map {
                Rates(it.base, mp, it.rates)
            }
            .retryWhen {
                it.delay(1, TimeUnit.SECONDS)
            }
            .repeatWhen {
                it.delay(1, TimeUnit.SECONDS, false)
            }
            .subscribe ({
                ratesNotifier.onNext(it)
            }, {  })
    }

}