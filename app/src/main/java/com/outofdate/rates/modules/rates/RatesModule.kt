package com.outofdate.rates.modules.rates

import dagger.Binds
import dagger.Module

@Module
interface RatesModule {

    @Binds
    @RatesContract.Scope
    fun presenter(presenter: RatesPresenterImpl): RatesContract.Presenter

    @Binds
    @RatesContract.Scope
    fun interactor(interactor: RatesInteractorImpl): RatesContract.Interactor

}
