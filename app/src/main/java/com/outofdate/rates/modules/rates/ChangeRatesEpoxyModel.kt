package com.outofdate.rates.modules.rates

import android.text.Editable
import android.text.TextWatcher
import com.outofdate.rates.R
import com.outofdate.rates.base.epoxy.BaseEpoxyModel
import com.outofdate.rates.base.internal.inject
import com.outofdate.rates.databinding.ItemChangeRatesBinding
import com.outofdate.rates.modules.rates.model.Rate
import javax.inject.Inject

class ChangeRatesEpoxyModel(data: Rate) : BaseEpoxyModel<Rate, ItemChangeRatesBinding>(data), TextWatcher {

    @Inject
    lateinit var presenter: RatesContract.Presenter

    override fun afterTextChanged(s: Editable?) {
        presenter.setMultiplier(s.toString().toFloatOrNull())
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
  1  }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

    }

    init {
        id(data.name)
    }

    override fun bind(binding: ItemChangeRatesBinding) {
        binding.value.removeTextChangedListener(this)
        binding.name.text = data.name
        inject(RatesComponent::class) {
            inject(this@ChangeRatesEpoxyModel)
        }
        binding.value.setText((data.mp).toString())
        binding.value.addTextChangedListener(this)
    }

    override fun unbind(binding: ItemChangeRatesBinding) {
        binding.value.removeTextChangedListener(this)
    }

    override fun getDefaultLayout() = R.layout.item_change_rates
}