package com.outofdate.rates.modules.rates

import com.outofdate.rates.base.internal.AppComponent
import com.outofdate.rates.base.internal.net.servicemodules.ApiModule
import dagger.Component

@RatesContract.Scope
@Component(dependencies = [AppComponent::class],
        modules = [
            RatesModule::class,
            ApiModule::class
        ]
)
interface RatesComponent {

    fun inject(fragment: RatesFragment)
    fun inject(model: ChangeRatesEpoxyModel)
    fun inject(model: RatesEpoxyModel)

}
