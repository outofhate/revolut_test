package com.outofdate.rates.modules.rates.model

data class Rates(
    val base: String,
    var multipliers: Float = 0f,
    val rates: HashMap<String, Float>
)