package com.outofdate.rates.modules.rates

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.airbnb.epoxy.TypedEpoxyController
import com.outofdate.rates.modules.rates.model.Rate
import com.outofdate.rates.modules.rates.model.Rates
import javax.inject.Inject

class RatesController @Inject constructor() : TypedEpoxyController<Rates>() {


    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        (recyclerView.layoutManager as LinearLayoutManager).recycleChildrenOnDetach = true
    }

    override fun buildModels(data: Rates?) {
        if (data != null)
            add(ChangeRatesEpoxyModel(Rate(data.base, data.multipliers, data.base, 0f, true)))

        for (key in data!!.rates.keys) {
            add(RatesEpoxyModel(Rate(data.base, data.multipliers, key, data.rates[key]!!, false)))
        }
    }

    fun setMultiplier(fl: Float) {
        if (currentData != null) {
            val copyOfRates = currentData!!.copy()
            if (copyOfRates.multipliers != fl) {
                copyOfRates.multipliers = fl
                setData(copyOfRates)
            }
        }
    }

}