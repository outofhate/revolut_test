package com.outofdate.rates.modules.rates

import com.outofdate.rates.base.mvp.Mvp
import com.outofdate.rates.modules.rates.model.Rates
import io.reactivex.Observable

interface RatesContract {

    interface View : Mvp.View {
        fun update(rates: Rates)
        fun setMultiplier(fl: Float)
        fun toTop()
    }

    interface Presenter : Mvp.Presenter<View, Interactor> {
        fun getRates(base: String)
        fun setMultiplier(mp: Float?)
    }

    interface Interactor : Mvp.Interactor {
        fun getRates(base:String)
        fun setMultiplier(m: Float)
        fun subscribeUpdates(): Observable<Rates>
    }


    @javax.inject.Scope
    @Retention(AnnotationRetention.RUNTIME)
    annotation class Scope
}

