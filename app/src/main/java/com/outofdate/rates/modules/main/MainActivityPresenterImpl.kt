package com.outofdate.rates.modules.main

import com.outofdate.rates.base.mvp.AbsMvpPresenter
import com.outofdate.rates.modules.main.MainActivityContract.*
import javax.inject.Inject

class MainActivityPresenterImpl @Inject constructor(interactor: Interactor) : AbsMvpPresenter<View, Interactor>(interactor), Presenter {

}