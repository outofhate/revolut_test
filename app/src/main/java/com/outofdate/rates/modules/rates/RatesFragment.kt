package com.outofdate.rates.modules.rates

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.outofdate.rates.R
import com.outofdate.rates.base.internal.inject
import com.outofdate.rates.base.mvp.MvpFragment
import com.outofdate.rates.databinding.FragmentRatesBinding
import com.outofdate.rates.modules.rates.model.Rates
import javax.inject.Inject

class RatesFragment : MvpFragment<FragmentRatesBinding, RatesContract.View, RatesContract.Presenter>(),
    RatesContract.View {
    @Inject
    lateinit var controller: RatesController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        inject(RatesComponent::class) {
            inject(this@RatesFragment)
        }
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rates.layoutManager = LinearLayoutManager(context)
        binding.rates.adapter = controller.adapter
        presenter.getRates("EUR")
    }

    override fun toTop() {
        binding.rates.postDelayed({
            binding.rates.layoutManager?.scrollToPosition(0)
        }, 500)
    }

    override fun update(rates: Rates) {
        controller.setData(rates)
    }

    override fun setMultiplier(fl: Float) {
        controller.setMultiplier(fl)

    }

    override val layout = R.layout.fragment_rates

}
