import com.android.build.gradle.ProguardFiles.getDefaultProguardFile
import org.apache.commons.logging.LogFactory.release
import org.gradle.internal.impldep.bsh.commands.dir
import org.gradle.internal.impldep.com.amazonaws.PredefinedClientConfigurations.defaultConfig
import org.gradle.internal.impldep.org.junit.experimental.categories.Categories.CategoryFilter.include
import org.jetbrains.kotlin.config.AnalysisFlag.Flags.experimental
import org.jetbrains.kotlin.gradle.dsl.Coroutines
import org.jetbrains.kotlin.kapt3.base.Kapt.kapt

val kotlin_version: String by parent!!.extra

buildscript {
    configure(listOf(repositories, project.repositories)) {
        gradlePluginPortal()
        google()
    }
}
plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-kapt")

}

apply {
    plugin("kotlin-android")
}
kotlin.experimental.coroutines = Coroutines.ENABLE

android {
    buildToolsVersion("28.0.3")
    compileSdkVersion(28)
    defaultConfig {
        multiDexEnabled = true
        applicationId = "com.outofdate.revolut"
        minSdkVersion(16)
        targetSdkVersion(28)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    }
    dataBinding {
        isEnabled = true
    }

    dexOptions {
        preDexLibraries = true
        maxProcessCount = 8
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
        getByName("debug") {
            isMinifyEnabled = false
            versionNameSuffix = "-SNAPSHOT"
        }
    }
    sourceSets {
        getByName("main").java.srcDirs("src/main/kotlin")
        getByName("debug").java.srcDirs("src/debug/kotlin")
    }

    splits {

        abi {
            isEnable = true
            reset()
            include("x86", "armeabi-v7a")
            isUniversalApk = false
        }
    }

    compileOptions {
        setSourceCompatibility(1.8)
        setTargetCompatibility(1.8)
    }
}

dependencies {

    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    testImplementation("junit:junit:4.12")
    androidTestImplementation("com.android.support.test.espresso:espresso-core:3.0.2")
    androidTestImplementation("com.android.support.test:runner:1.0.2")
    implementation("com.facebook.fresco:imagepipeline-okhttp3:1.10.0")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version")
    implementation("com.android.support:appcompat-v7:28.0.0")
    implementation("com.android.support:recyclerview-v7:28.0.0")
    implementation("com.android.support:support-core-utils:28.0.0")
    implementation("com.android.support:design:28.0.0")
    implementation("com.squareup.retrofit2:retrofit:2.5.0")
    implementation("com.squareup.okhttp3:logging-interceptor:3.10.0")
    implementation("com.google.code.gson:gson:2.8.5")
    implementation("com.squareup.retrofit2:converter-gson:2.5.0")
    implementation("com.squareup.retrofit2:adapter-rxjava2:2.5.0")
    implementation("com.airbnb.android:epoxy:2.14.0")
    implementation("com.airbnb.android:epoxy-databinding:2.14.0")
    implementation("com.jakewharton.timber:timber:4.7.1")
    implementation("com.android.support:multidex:1.0.3")
    implementation("com.google.dagger:dagger:2.17")
    implementation("io.reactivex.rxjava2:rxjava:2.2.4")
    implementation("io.reactivex.rxjava2:rxandroid:2.1.0")
    implementation("com.google.code.findbugs:jsr305:3.0.2")

    kapt("com.google.dagger:dagger-compiler:2.17")

    implementation(kotlin("reflect", "1.2.61"))

    implementation("com.android.support.constraint:constraint-layout:2.0.0-alpha2")

}
repositories {
    mavenCentral()
}