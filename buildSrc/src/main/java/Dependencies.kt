import Versions.crashlyticsVer
import Versions.jodaTimeVer
import Versions.kotlinVer
import Versions.retrofitVer
import Versions.rxjavaAndroidVer
import Versions.rxjavaVer
import Versions.supportVer
import Versions.ttbpVer

object Versions {
    val kotlinVer = "1.2.61"
    val supportVer = "28.0.0"
    val retrofitVer = "2.3.0"
    val rxjavaVer = "2.2.2"
    val rxjavaAndroidVer = "2.1.0"
    val ttbpVer = "1.1.0"
    val jodaTimeVer = "2.9.9"
    val crashlyticsVer = "2.9.4@aar"
}

object Dependencies {
    val stdlib = "org.jetbrains.kotlin:kotlin-stdlib:$kotlinVer"
    val supportAnnotations = "com.android.support:support-annotations:$supportVer"
    val supportAppcompatV7 = "com.android.support:appcompat-v7:$supportVer"
    val retrofit = "com.squareup.retrofit2:retrofit:$retrofitVer"
    val retrofitRxjavaAdapter = "com.squareup.retrofit2:adapter-rxjava2:$retrofitVer"
    val rxjava = "io.reactivex.rxjava2:rxjava:$rxjavaVer"
    val rxjavaAndroid = "io.reactivex.rxjava2:rxandroid:$rxjavaAndroidVer"
    val ttbp = "com.jakewharton.threetenabp:threetenabp:$ttbpVer"
    val jodaTime = "joda-time:joda-time:$jodaTimeVer"
    val crashlytics = "com.crashlytics.sdk.android:crashlytics:$crashlyticsVer"
}

